# This file is your Lambda function
import json


def mirror_query_strings(event, context):
    body = {
        "queryStrings": event['queryStringParameters']
    }
    return {
        "statusCode": 200,
        "body": json.dumps(body)
    }
